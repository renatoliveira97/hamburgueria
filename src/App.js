import './App.css';
import { useState } from 'react';

import MenuContainer from './components/MenuContainer';

function App() {

  const [products] = useState([
    { id: 1, name: 'Hamburguer', category: 'Sanduíches', price: 7.99 },
    { id: 2, name: 'X-Burguer', category: 'Sanduíches', price: 8.99 },
    { id: 3, name: 'X-Salada', category: 'Sanduíches', price: 10.99 },
    { id: 4, name: 'Big Kenzie', category: 'Sanduíches', price: 16.99 },
    { id: 5, name: 'Guaraná', category: 'Bebidas', price: 4.99 },
    { id: 6, name: 'Coca', category: 'Bebidas', price: 4.99 },
    { id: 7, name: 'Fanta', category: 'Bebidas', price: 4.99 },
  ]); 

  const [filteredProducts, setFilteredProducts] = useState([...products]);

  const [currentSale, setCurrentSale] = useState([]);
  const [cartTotal, setCartTotal] = useState(0);
  const [inputValue, setInputValue] = useState("");

  const ShowProducts = (event, input) => {
    event.preventDefault();
    setFilteredProducts(products.filter(x => x.name.toLowerCase().includes(input.toLowerCase())));
    setInputValue("");
  }

  const HandleClick = (productId) => {
    let arr = products.filter(item => item.id === productId);
    setCurrentSale([...currentSale, arr]);
  }
  
  return (
    <div className="App">
      <header className="App-header">
        <form>
          <input
              type="text"
              value={inputValue}
              onChange={event => setInputValue(event.target.value)}
              />
          <button onClick={event => ShowProducts(event, inputValue)}>Pesquisar</button>
        </form>
        <MenuContainer filteredProducts={filteredProducts} currentSale={currentSale}
                       cartTotal={cartTotal} setCartTotal={setCartTotal} HandleClick={HandleClick}/>   
      </header>
    </div>
  );
}

export default App;
