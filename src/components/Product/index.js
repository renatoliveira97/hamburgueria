import './style.css';

const Product = ({ item, cartTotal, setCartTotal, currentSale, HandleClick }) => {

    const HandleButtonClick = (product) => {

        let duplicated = false;
        let item = currentSale.map(x => x[0].name);

        for (let i = 0; i < item.length; i++) {
            if (product.name === item[i]) {
                duplicated = true;
            }
        }

        if (!duplicated) {
            HandleClick(product.id);
            setCartTotal(cartTotal + product.price);
        }

    }

    return (
        <div>
            <p className="name">{item.name}</p>
            <p>Categoria - {item.category}</p>
            <p className="price">Preço - {item.price} R$</p>
            <button onClick={() => HandleButtonClick(item)}>Adicionar</button>
        </div>
    );
}

export default Product;