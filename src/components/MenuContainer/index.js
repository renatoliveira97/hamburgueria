import './style.css';

import Product from "../Product";

const MenuContainer = ({ filteredProducts, currentSale, cartTotal, setCartTotal, HandleClick}) => {
    return (
        <div className="menu">
            <div className="listaDeProdutos">
                {filteredProducts.map((item, index) => (
                    <Product key={index} item={item} cartTotal={cartTotal} setCartTotal={setCartTotal} 
                             currentSale={currentSale} HandleClick={HandleClick}/>
                ))}
            </div>            
            <p className="subtotal">Subtotal - {cartTotal} R$</p>
            <div className="carrinho">
                {currentSale.map((item, index) => (
                    <div key={index}>
                        <p className="name">{item[0].name}</p>
                        <p>Categoria - {item[0].category}</p>
                        <p className="price">Preço - {item[0].price} R$</p>
                    </div>
                ))}
            </div>            
        </div>
    );
}

export default MenuContainer;